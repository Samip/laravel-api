<?php

namespace Database\Seeders;

use App\Models\DocumentType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DocumentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DocumentType::insert(
            [
                ['type' => 'Citizenship'],
                ['type' => 'Passport'],
            ]
        );
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
