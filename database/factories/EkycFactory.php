<?php

namespace Database\Factories;

use App\Models\Gender;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Ekyc>
 */
class EkycFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'first_name' => fake()->firstName(),
            'last_name' => fake()->lastName(),
            'dob' => fake()->date(),
            'gender' => array_rand(Gender::options()),
            'document_type' => '1',
            'document_number' => Str::random(10),
            'document_image' => UploadedFile::fake()->image('document.jpg'),
            'pan' => Str::random(10),
            'city' => fake()->city(),
            'street' => fake()->streetAddress(),
            'contact_number' => fake()->phoneNumber(),
        ];
    }
}
