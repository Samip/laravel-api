<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Illuminate\Support\Str;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function test_login_with_valid_credentials()
    {
        $user = User::factory()->create();

        $response = $this->postJson('/api/v1/auth/login', [
            'email' => $user->email,
            'password' => 'password',
            'device_name' => 'mobile'
        ]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'access_token',
                'token_type',

            ]);
    }

    public function test_login_with_incorrect_email()
    {
        $user = User::factory()->create();

        $response = $this->postJson('/api/v1/auth/login', [
            'email' => 'wrongemail@example.com',
            'password' => 'password',
            'device_name' => 'mobile'
        ]);

        $response->assertStatus(422)
            ->assertJson([
                'message' => 'The provided credentials are incorrect.',
                'errors' => [
                    'email' => ['The provided credentials are incorrect.'],
                ],
            ]);
    }

    public function test_login_with_incorrect_password()
    {
        $user = User::factory()->create();

        $response = $this->postJson('/api/v1/auth/login', [
            'email' => $user->email,
            'password' => 'wrongpassword',
            'device_name' => 'mobile'
        ]);

        $response->assertStatus(422)
            ->assertJson([
                'message' => 'The provided credentials are incorrect.',
                'errors' => [
                    'email' => ['The provided credentials are incorrect.'],
                ],
            ]);
    }

    public function test_user_can_register()
    {
        $userData = [
            'name' => fake()->name(),
            'email' => fake()->unique()->safeEmail(),
            'password' => 'Password@123',
            'password_confirmation' => 'Password@123',
            'device_name' => 'mobile'
        ];

        $response = $this->postJson('/api/v1/auth/register', $userData);

        $response->assertStatus(200);

        $this->assertDatabaseHas('users', [
            'email' => $userData['email'],
        ]);
    }

    public function test_register_for_invalid_password()
    {
        $userData = [
            'name' => fake()->name(),
            'email' => fake()->unique()->safeEmail(),
            'password' => 'Password123',
            'password_confirmation' => 'Password123',
            'device_name' => 'mobile'
        ];

        $response = $this->postJson('/api/v1/auth/register', $userData);

        $response->assertStatus(422)->assertJsonValidationErrors(['password']);


        $userData = [
            'name' => fake()->name(),
            'email' => fake()->unique()->safeEmail(),
            'password' => 'Password@!',
            'password_confirmation' => 'Password@!',
            'device_name' => 'mobile'
        ];

        $response = $this->postJson('/api/v1/auth/register', $userData);

        $response->assertStatus(422)->assertJsonValidationErrors(['password']);


        $userData = [
            'name' => fake()->name(),
            'email' => fake()->unique()->safeEmail(),
            'password' => 'password@123',
            'password_confirmation' => 'password@123',
            'device_name' => 'mobile'
        ];

        $response = $this->postJson('/api/v1/auth/register', $userData);

        $response->assertStatus(422)->assertJsonValidationErrors(['password']);


        $userData = [
            'name' => fake()->name(),
            'email' => fake()->unique()->safeEmail(),
            'password' => 'PASSWORD@123',
            'password_confirmation' => 'PASSWORD@123',
            'device_name' => 'mobile'
        ];

        $response = $this->postJson('/api/v1/auth/register', $userData);

        $response->assertStatus(422)->assertJsonValidationErrors(['password']);


        $userData = [
            'name' => fake()->name(),
            'email' => fake()->unique()->safeEmail(),
            'password' => 'Password@123',
            'password_confirmation' => 'Password123',
            'device_name' => 'mobile'
        ];

        $response = $this->postJson('/api/v1/auth/register', $userData);

        $response->assertStatus(422)->assertJsonValidationErrors(['password']);
    }


    public function test_register_for_duplicate_email()
    {
        $email = fake()->unique()->safeEmail();

        $userData = [
            'name' => fake()->name(),
            'email' => $email,
            'password' => 'Password@123',
            'password_confirmation' => 'Password@123',
            'device_name' => 'mobile'
        ];

        $response = $this->postJson('/api/v1/auth/register', $userData);

        $response->assertStatus(200);

        $userData = [
            'name' => fake()->name(),
            'email' => $email,
            'password' => 'Password@123',
            'password_confirmation' => 'Password@123',
            'device_name' => 'mobile'
        ];

        $response = $this->postJson('/api/v1/auth/register', $userData);

        $response->assertStatus(422)->assertJsonValidationErrors(['email']);
    }

    public function test_authenticated_user_can_change_password()
    {
        $user = User::factory()->create();

        $token = $user->createToken('mobile')->plainTextToken;

        $newPassword = 'Password#123';

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->post('api/v1/auth/change-password', [
                'current_password' => 'password',
                'password' => $newPassword,
                'password_confirmation' => $newPassword
            ]);

        $response->assertStatus(200);

        $this->assertTrue(Hash::check($newPassword, $user->fresh()->password));
    }

    public function test_Unauthenticated_user_can_change_password()
    {

        $newPassword = 'Password#123';

        $response = $this->postJson('api/v1/auth/change-password', [
            'current_password' => 'password',
            'password' => $newPassword,
            'password_confirmation' => $newPassword
        ]);

        $response->assertStatus(401);
    }

    public function test_change_password_with_invalid_current_password()
    {
        $user = User::factory()->create();

        $token = $user->createToken('mobile')->plainTextToken;

        $newPassword = 'Password#123';

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->post('api/v1/auth/change-password', [
                'current_password' => 'abcdef',
                'password' => $newPassword,
                'password_confirmation' => $newPassword
            ]);

        $response->assertStatus(422);
    }

    public function test_change_password_with_invalid_password()
    {
        $user = User::factory()->create();

        $token = $user->createToken('mobile')->plainTextToken;

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->post('api/v1/auth/change-password', [
                'current_password' => 'password',
                'password' => 'Password123',
                'password_confirmation' => 'Password123'
            ]);

        $response->assertStatus(422)->assertJsonValidationErrorFor('password');

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->post('api/v1/auth/change-password', [
                'current_password' => 'password',
                'password' => 'Password@!',
                'password_confirmation' => 'Password@1'
            ]);

        $response->assertStatus(422)->assertJsonValidationErrorFor('password');

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->post('api/v1/auth/change-password', [
                'current_password' => 'password',
                'password' => 'password@123',
                'password_confirmation' => 'password@123'
            ]);

        $response->assertStatus(422)->assertJsonValidationErrorFor('password');

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->post('api/v1/auth/change-password', [
                'current_password' => 'password',
                'password' => 'Password@123',
                'password_confirmation' => 'Password123'
            ]);

        $response->assertStatus(422)->assertJsonValidationErrorFor('password');

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->post('api/v1/auth/change-password', [
                'current_password' => 'password',
                'password' => 'Pass@12',
                'password_confirmation' => 'Pass@12'
            ]);

        $response->assertStatus(422)->assertJsonValidationErrorFor('password');
    }

    public function test_user_can_refresh_access_token()
    {
        $user = User::factory()->create();

        $deviceName = "mobile";

        $token = $user->createToken($deviceName)->plainTextToken;

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])->postJson('/api/v1/auth/refresh', [
                'device_name' => $deviceName
            ]);

        $response->assertOk()->assertJsonStructure([
            'access_token',
            'token_type',
            'user',
        ]);
    }

    public function test_unauthenticated_user_can_refresh_access_token()
    {
        $deviceName = "mobile";

        $response = $this->postJson('/api/v1/auth/refresh', [
            'device_name' => $deviceName
        ]);

        $response->assertStatus(401);
    }

    public function test_user_cannot_refresh_token_with_invalid_access_token()
    {
        $user = User::factory()->create();

        $deviceName = "mobile";

        $token = $user->createToken($deviceName)->plainTextToken;

        $token = Str::random(30);

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->postJson('/api/v1/auth/refresh', ['device_name' => $deviceName]);


        $response->assertUnauthorized()
            ->assertJsonStructure(['message']);
    }

    public function test_user_cannot_refresh_token_Without_access_token()
    {
        $user = User::factory()->create();

        $deviceName = "mobile";

        $token = $user->createToken($deviceName)->plainTextToken;

        // Delete the refresh token
        DB::table('personal_access_tokens')
            ->where('tokenable_id', $user->id)
            ->delete();

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
            ])
            ->postJson('/api/v1/auth/refresh', ['device_name' => $deviceName]);

        $response->assertUnauthorized()
            ->assertJsonStructure(['message']);
    }

    public function test_authenticated_user_can_logout()
    {
        $user = User::factory()->create();

        $deviceName = "mobile";

        $token = $user->createToken($deviceName)->plainTextToken;

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->post('api/v1/auth/logout');

        $response->assertNoContent();
        $this->assertDatabaseMissing('personal_access_tokens', [
            'tokenable_id' => $user->id,
        ]);
    }

    public function test_unauthenticated_user_cannot_logout()
    {
        $response = $this->postJson('api/v1/auth/logout');

        $response->assertStatus(401);
    }

    public function test_logout_revokes_all_tokens_for_user()
    {
        $user = User::factory()->create();

        $token1 = $user->createToken('Test Token 1')->plainTextToken;
        $token2 = $user->createToken('Test Token 2')->plainTextToken;

        $response = $this->actingAs($user)
            ->withHeaders([
                'Authorization' => "Bearer $token1",
                'Accept' => 'application/json'
            ])
            ->post('api/v1/auth/logout');

        $response->assertStatus(204);
        $this->assertNull($user->tokens()->first());

        $this->assertFalse($user->tokens()->where('id', $token1)->exists());
        $this->assertFalse($user->tokens()->where('id', $token2)->exists());
    }
}
