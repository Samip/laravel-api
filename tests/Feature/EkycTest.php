<?php

namespace Tests\Feature;

use App\Models\DocumentType;
use App\Models\Ekyc;
use App\Models\Gender;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Support\Str;

class EkycTest extends TestCase
{

    use RefreshDatabase;


    public function test_get_all_data()
    {

        $user = User::factory()->create();

        $token = $user->createToken('Test Token')->plainTextToken;

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->getJson('api/v1/ekyc');
        $response->assertStatus(200);
    }

    public function test_authenticated_user_can_store_kyc_information()
    {
        $user = User::factory()->create();

        $documentType = DocumentType::factory()->create();

        $token = $user->createToken('Test Token')->plainTextToken;

        $ekyc = Ekyc::factory()->make(['document_type' => $documentType->id]);


        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->post('api/v1/ekyc',  $ekyc->toArray());

        $response->assertCreated();

        Storage::disk('public')->exists($response->json('data.document_image'));
    }


    public function test_unauthenticated_user_cannot_store_kyc_information()
    {

        $documentType = DocumentType::factory()->create();


        $ekyc = Ekyc::factory()->make(['document_type' => $documentType->id]);

        $response = $this->postJson('api/v1/ekyc',  [$ekyc->toArray()]);

        $response->assertUnauthorized();
    }

    public function test__required_field()
    {
        $user = User::factory()->create();

        $documentType = DocumentType::factory()->create();

        $token = $user->createToken('Test Token')->plainTextToken;

        $ekyc = Ekyc::factory()->make([
            'first_name' => '',
            'last_name' => '',
            'dob' => '',
            'gender' => '',
            'document_type' => '',
            'document_number' => '',
            'document_image' => '',
            'pan' => '',
            'city' => '',
            'street' => '',
            'contact_number' => '',
        ]);

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->post('api/v1/ekyc',  $ekyc->toArray());

        $response->assertStatus(422)
            ->assertJsonValidationErrors([
                'first_name',
                'last_name',
                'dob',
                'gender',
                'document_type',
                'document_number',
                'document_image',
                'pan',
                'city',
                'street',
            ]);
    }

    public function test_it_requires_a_valid_document_image()
    {
        $user = User::factory()->create();

        $documentType = DocumentType::factory()->create();

        $token = $user->createToken('Test Token')->plainTextToken;

        $ekyc = Ekyc::factory()->make(['document_type' => $documentType->id, 'document_image' => '']);

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->post('api/v1/ekyc',  $ekyc->toArray());


        $response->assertStatus(422)
            ->assertJsonValidationErrors(['document_image']);
    }

    public function test_document_image_invalid_image()
    {
        $user = User::factory()->create();

        $documentType = DocumentType::factory()->create();

        $token = $user->createToken('Test Token')->plainTextToken;

        $ekyc = Ekyc::factory()->make(['document_type' => $documentType->id, 'document_image' => UploadedFile::fake()->create('test.pdf', 100)]);

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->post('api/v1/ekyc',  $ekyc->toArray());


        $response->assertStatus(422)
            ->assertJsonValidationErrors(['document_image']);
    }

    public function test_document_image_file_size_validation()
    {
        $user = User::factory()->create();

        $documentType = DocumentType::factory()->create();

        $token = $user->createToken('Test Token')->plainTextToken;

        $ekyc = Ekyc::factory()->make(['document_type' => $documentType->id, 'document_image' => UploadedFile::fake()->create('logo.jpg', 4000)]);

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->post('api/v1/ekyc',  $ekyc->toArray());


        $response->assertStatus(422)
            ->assertJsonValidationErrors(['document_image']);
    }

    public function test_invalid_gender()
    {
        $user = User::factory()->create();

        $documentType = DocumentType::factory()->create();

        $token = $user->createToken('Test Token')->plainTextToken;

        $ekyc = Ekyc::factory()->make(['document_type' => $documentType->id, 'gender' => 40]);

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->post('api/v1/ekyc',  $ekyc->toArray());


        $response->assertStatus(422)
            ->assertJsonValidationErrors(['gender']);
    }

    public function test_invalid_document_type()
    {
        $user = User::factory()->create();

        $token = $user->createToken('Test Token')->plainTextToken;

        $ekyc = Ekyc::factory()->make(['document_type' => rand(2, 10)]);

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->post('api/v1/ekyc',  $ekyc->toArray());


        $response->assertStatus(422)
            ->assertJsonValidationErrors(['document_type']);
    }

    public function test_invalid_contact_number()
    {
        $user = User::factory()->create();

        $documentType = DocumentType::factory()->create();

        $token = $user->createToken('Test Token')->plainTextToken;

        $ekyc = Ekyc::factory()->make(['document_type' => $documentType->id, 'contact_number' => 40000]);

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->post('api/v1/ekyc',  $ekyc->toArray());


        $response->assertStatus(422)
            ->assertJsonValidationErrors(['contact_number']);

        $ekyc = Ekyc::factory()->make(['document_type' => $documentType->id, 'contact_number' => Str::random(25)]);

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->post('api/v1/ekyc',  $ekyc->toArray());


        $response->assertStatus(422)
            ->assertJsonValidationErrors(['contact_number']);
    }

    public function test_authenticated_user_can_update_kyc_information()
    {
        $user = User::factory()->create();

        $documentType = DocumentType::factory()->create();

        $token = $user->createToken('Test Token')->plainTextToken;

        $ekyc = Ekyc::factory()->make(['document_type' => $documentType->id,]);

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->post('api/v1/ekyc',  $ekyc->toArray());

        $response->assertCreated();

        $firstName = 'ram';

        $newEkycData = $ekyc->toArray();

        $newEkycData['first_name'] = $firstName;
        $newEkycData['document_image'] = UploadedFile::fake()->create('test.jpg', 100);
        $newEkycData['_method'] = "PUT";

        $id = $response->json('data.id');

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->post("api/v1/ekyc/{$id}",  $newEkycData);

        $response->assertOk()->assertJsonFragment([
            'first_name' => $firstName,
        ]);
    }

    public function test_get_data()
    {

        $user = User::factory()->create();

        $documentType = DocumentType::factory()->create();

        $token = $user->createToken('Test Token')->plainTextToken;

        $ekyc = Ekyc::factory()->make(['document_type' => $documentType->id,]);

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->post('api/v1/ekyc',  $ekyc->toArray());

        $response->assertCreated();

        $id = $response->json('data.id');

        $returnedData = $response->json('data');

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->get("api/v1/ekyc/{$id}");

        $response->assertOk()->assertJsonFragment($returnedData);
    }


    public function test_get_all_document_type()
    {

        $user = User::factory()->create();

        $documentType = DocumentType::factory(2)->create();

        $token = $user->createToken('Test Token')->plainTextToken;

        $documentTypeData = DocumentType::select('id', 'type')->get()->toArray();

        $data['data'] = $documentTypeData;

        $response = $this
            ->withHeaders([
                'Authorization' => "Bearer $token",
                'Accept' => 'application/json',
                'Content' => 'application/json'
            ])
            ->get("api/v1/document-type/");

        $response->assertOk()->assertJson($data);
    }
}
