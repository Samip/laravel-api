<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('v1')->group(function () {

    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::post('/auth/refresh', \App\Http\Controllers\API\Auth\RefreshTokenController::class);
        Route::post('/auth/change-password', \App\Http\Controllers\API\Auth\ChangePasswordController::class);
        Route::post('/auth/logout', [\App\Http\Controllers\API\Auth\LogoutController::class, 'logout']);

        Route::apiResource('ekyc', \App\Http\Controllers\API\EkycController::class);
        Route::get('document-type', [\App\Http\Controllers\API\DocumentTypeController::class, 'index']);
    });

    Route::post('/auth/login', [\App\Http\Controllers\API\Auth\LoginController::class, 'login']);
    Route::post('/auth/register', [\App\Http\Controllers\API\Auth\RegisterController::class, 'register']);
});
