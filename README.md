<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

# Versions

## php 8.1

#

# Setup

-   Clone the repo
-   Run `composer install`
-   Copy **.env.example** to **.env** and fill in database details
-   Run `php artisan key:generate`
-   Run `php artisan migrate`
-   Run `php artisan storage:link`
-   Run `php artisan db:seed`

#

# Test Case
Run `php artisan test`

# APIs

## Login
POST /api/v1/auth/login
Content-Type: application/json

## register
POST /api/v1/auth/register
Content-Type: application/json

## change password
POST /api/v1/auth/change-password
Content-Type: application/json
Authorization: Bearer <your-token>

## refresh
POST /api/v1/auth/refresh
Content-Type: application/json
Authorization: Bearer <your-token>

## logout
POST /api/v1/auth/logout
Content-Type: application/json
Authorization: Bearer <your-token>

## get all ekyc
GET /api/v1/ekyc
Content-Type: application/json
Authorization: Bearer <your-token>

## Create ekyc
POST /api/v1/ekyc
Content-Type: application/json
Authorization: Bearer <your-token>

## show ekyc details
GET /api/v1/ekyc/{id}
Content-Type: application/json
Authorization: Bearer <your-token>

## update ekyc
PUT /api/v1/ekyc
Content-Type: application/json
Authorization: Bearer <your-token>

## get document_type
GET /api/v1/document-type
Content-Type: application/json
Authorization: Bearer <your-token>

## ekyc data
[
'first_name',
'last_name',
'dob',
'gender',
'document_type',
'document_number',
'document_image'
'pan',
'city',
'street',
'contact_number'
]

## document type data
['type']


##  Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

##  License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
