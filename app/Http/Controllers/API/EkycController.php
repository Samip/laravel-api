<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\EkycRequest;
use App\Http\Resources\AllEkycResource;
use App\Http\Resources\EkycResource;
use App\Models\Ekyc;
use App\Services\EkycService;

class EkycController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return AllEkycResource::collection(
            Ekyc::with('documentType')->select('first_name', 'last_name', 'document_type', 'document_number', 'contact_number')->get()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  EkycRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EkycRequest $request)
    {
        $ekyc = (new EkycService())->store($request->validated(), $request->file('document_image'));

        return new EkycResource($ekyc);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ekyc  $%ekyc
     * @return \Illuminate\Http\Response
     */
    public function show(Ekyc $ekyc)
    {

        return new EkycResource($ekyc);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EkycRequest  $request
     * @param  \App\Models\Ekyc  $ekyc
     * @return \Illuminate\Http\Response
     */
    public function update(EkycRequest $request, Ekyc $ekyc)
    {
        $documentImage = $request->file('document_image');

        $ekyc = (new EkycService())->update($request->validated(), $ekyc, $documentImage);

        return new EkycResource($ekyc);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ekyc  $%ekyc
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ekyc $ekyc)
    {
        $ekyc->delete();

        return response()->noContent();
    }
}
