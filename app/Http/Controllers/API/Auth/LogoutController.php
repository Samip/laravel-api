<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class LogoutController extends Controller
{
    public function logout()
    {
        $user = auth()->user();
      
        if ($user) {
            $user->tokens()->delete();
        }

        return response()->noContent();
    }
}
