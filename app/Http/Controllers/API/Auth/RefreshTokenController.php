<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RefreshTokenRequest;
use Illuminate\Http\Request;

class RefreshTokenController extends Controller
{
    public function __invoke(RefreshTokenRequest $request)
    {
        $user = auth()->user();

        $user->tokens()->delete();

        return response()->json([
            'access_token' => $user->createToken($request->input('device_name'))->plainTextToken,
            'token_type' => 'Bearer',
            'user' => $user
        ]);
    }
}
