<?php

namespace App\Http\Requests;

use App\Models\Gender;
use Illuminate\Foundation\Http\FormRequest;

class EkycRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
       return [
            'first_name' => 'required|string|min:2|max:255',
            'last_name' => 'required|string|min:2|max:255',
            'dob' => 'required|date',
            'gender' => ['required', 'numeric', \Illuminate\Validation\Rule::in(array_keys(Gender::options()))],
            'document_type' => 'required|exists:document_types,id',
            'document_number' => 'required|string|min:2|max:255',
            'document_image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'pan' => 'required|string|min:2|max:30',
            'city' => 'required|string|min:2|max:255',
            'street' => 'required|string|min:2|max:255',
            'contact_number' => 'required|string|min:8|max:20',
        ];
    }
}
