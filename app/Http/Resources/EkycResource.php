<?php

namespace App\Http\Resources;

use App\Models\Gender;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class EkycResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'dob' => $this->dob,
            'gender' => [
                'id' => $this->gender,
                'value' => Gender::options()[$this->gender]
            ],
            'document_type' => [
                'id' => $this->document_type,
                'type' => $this->documentType->type
            ],
            'document_number' => $this->document_number,
            'document_image' => $this->getMediaUrl(),
            'pan' => $this->pan,
            'city' => $this->city,
            'street' => $this->street,
            'contact_number' => $this->contact_number
        ];
    }

    private function getMediaUrl(): ?string
    {
        $media = $this->getFirstMedia('document_image');

        if ($media instanceof Media) {
            return $media->getUrl();
        }

        return null;
    }
}
