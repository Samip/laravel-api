<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Ekyc extends Model implements HasMedia
{
    use HasFactory, SoftDeletes, InteractsWithMedia;

    protected $fillable = [
        'first_name',
        'last_name',
        'dob',
        'gender',
        'document_type',
        'document_number',
        'pan',
        'city',
        'street',
        'contact_number'
    ];

    public function documentType()
    {
        return $this->belongsTo(DocumentType::class, 'document_type');
    }
}
