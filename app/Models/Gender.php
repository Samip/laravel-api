<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    public static function options()
    {
        return [
            0 => 'Male',
            1 => 'Female',
            2 => 'Other',
        ];
    }
}
