<?php

namespace App\Services;

use App\Models\Ekyc;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\ValidationException;

class EkycService
{

    public function store(array $ekycData, UploadedFile $documentImage): Ekyc
    {

        if (auth()->user()->ekyc)
            throw ValidationException::withMessages(['user_id' => 'eKYC is already filled for this user']);

        $ekyc = auth()->user()->ekyc()->create($ekycData);

        $ekyc->addMedia($documentImage)->toMediaCollection('document_image');

        return $ekyc;
    }

    public function update(array $ekycData, Ekyc $ekyc, ?UploadedFile $documentImage = null): Ekyc
    {
        
        $ekyc->update($ekycData);

        if ($documentImage !== null) {
            $media = $ekyc->getFirstMedia('document_image');

            if ($media) {
                $media->delete();
            }

            $ekyc->addMedia($documentImage)->toMediaCollection('document_image');
        }

        return $ekyc;
    }
}
